#!/bin/bash

# Uncomment the following line if clock is not already in /dev
#echo ds1307 0x68 > /sys/class/i2c-adapter/i2c-1/new_device

#get time from RTC
hwclock -s -f /dev/rtc1
#get current time in seconds
secs=$(date +%s)
default="1511296905"

#if time is obviously invalid, set it to the default one
if [ $secs -lt $default ]; then
  echo "Invalid time found, setting to $compare"
  date --date="@$default"
fi
#now try and sync from ntp
/usr/sbin/ntpdate -b -s -u pool.ntp.org
#write to external RTC
hwclock -w -f /dev/rtc1
#write to system RTC
hwclock -w

echo "Clocks updated"
